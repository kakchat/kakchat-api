package kz.kakchat.register;

import io.grpc.stub.StreamObserver;
import kz.kakchat.AsdServiceGrpc;
import kz.kakchat.Hello;

import java.util.Date;

public class AsdServiceImpl extends AsdServiceGrpc.AsdServiceImplBase {

  @Override
  public void asd(Hello.Request request, StreamObserver<Hello.Response> responseObserver) {

    Hello.Response response = Hello.Response.newBuilder()
                                            .setAsd("asd " + new Date())
                                            .build();

    System.out.println("w1uZu1lF8v :: WOW ");

    responseObserver.onNext(response);
    responseObserver.onCompleted();
  }
}
