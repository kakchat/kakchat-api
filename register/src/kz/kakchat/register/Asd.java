package kz.kakchat.register;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class Asd {

  public static void main(String[] args) throws IOException, InterruptedException {

    Server server = ServerBuilder.forPort(30999)
                                 .addService(new AsdServiceImpl())
                                 .build();

    server.start();

    System.out.println("31zRl7fZ4q :: Server started on port " + server.getPort());


    Thread serverThread = Thread.currentThread();

    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        System.out.println("3AtYKeYM4h :: SHUTDOWN");
        serverThread.interrupt();
        server.shutdown();
      }
    });

    server.awaitTermination();
  }

}
